package geometry;
import static org.junit.Assert.*;
import org.junit.Test;

public class testRectangle {
    
    @Test
    public void testConstructor() {
        Rectangle r = new Rectangle(2, 3);
        assertEquals(2, r.getLength(), 0.000001);
        assertEquals(3, r.getWidth(), 0.00001);
    }

    @Test
    public void testArea() {
        Rectangle r = new Rectangle(2,3);
        assertEquals(6, r.getArea(),0.00000000001);
    }

    @Test 
    public void testToString() {
        Rectangle r = new Rectangle(2,3);
       assertEquals("Length: 2.0 Width: 3.0", r.toString());
    }
}
