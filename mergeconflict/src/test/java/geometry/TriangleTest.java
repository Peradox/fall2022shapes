package geometry;
import static org.junit.Assert.*;
import org.junit.Test;

public class TriangleTest {
    final double deltolerance=0.0000000000000001;
    @Test
    public void testGetMethods(){
        Triangle t= new Triangle(4, 6);
        assertEquals(4,t.getBase(), deltolerance);
        assertEquals(6,t.getHeight(), deltolerance);
    }

    @Test
    public void testGetArea(){
        Triangle t= new Triangle(5, 8);
        assertEquals(20, t.getArea(), deltolerance);
    }

    @Test
    public void testToString(){
        Triangle t= new Triangle(3, 4);
        assertEquals("Base: 3.0 Height: 4.0 Area: 6.0", t.toString());
    }
}
