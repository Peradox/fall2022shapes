package geometry;

public class Shapes {
    public static void main(String[] args){
        Triangle triangle= new Triangle(3, 4);
        System.out.println(triangle);

        Rectangle r = new Rectangle(2, 3);
        System.out.println(r);
        System.out.println("Area: " + r.getArea());
    }
}
